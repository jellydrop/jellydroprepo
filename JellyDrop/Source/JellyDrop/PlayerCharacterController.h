// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "PlayerCharacterController.generated.h"

namespace Dir
{
	enum Type
	{
		Right, Up, Left, Down, None
	};
};
/**
 * 
 */
UCLASS()
class JELLYDROP_API APlayerCharacterController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void SetupInputComponent() override;

	void Face(Dir::Type dir);
	void MoveForward(float value);
	void MoveRight(float value);
	void Attack();

	virtual void PlayerTick(float DeltaTime) override;

	void CheckRest();
	void Forward();
	bool CheckForward();

	void Dash();
	void DashForward();

	bool mRest = true;
	bool mMoving = false;

	float mDeadsone = 0.1f;
	float mLastFwd = 0.0f;
	float mLastRight = 0.0f;
	
	Dir::Type mFacing = Dir::Up;

	int mDash = 8;
	
public:

	void ResetGame();
	void QuitGame();
	
	FHitResult DoTrace(FVector start, FVector end);
};
