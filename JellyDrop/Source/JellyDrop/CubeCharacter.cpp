
// Fill out your copyright notice in the Description page of Project Settings.

#include "JellyDrop.h"
#include "CubeCharacter.h"


// Sets default values
ACubeCharacter::ACubeCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACubeCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACubeCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ACubeCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void ACubeCharacter::SquareHit()
{
	if (touched)
		return;

	touched = true;

	if (curCol == Color::even || curCol == Color::odd)
		rCurCol = curCol;

	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ACubeCharacter::DropThisSquare, 1.0f);
	ChangeColor(1);

	GetWorldTimerManager().SetTimer(mResetHandle, this, &ACubeCharacter::ResetPosition, 5.0f);
}

void ACubeCharacter::DropThisSquare(){
    this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
    
   // FTimerHandle handle;
    //GetWorldTimerManager().SetTimer(handle, this, &ACubeCharacter::ResetPosition, 10.0f);
}

void ACubeCharacter::DropThisSquarePerm(){
	this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);

	GetWorldTimerManager().ClearTimer(mResetHandle);
}

void ACubeCharacter::ResetPosition(){
	GetWorldTimerManager().ClearTimer(mResetHandle);
	this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
    FRotator rot = FRotator(0,0,0);
    this->TeleportTo(location, rot);
	ChangeColor(rCurCol);

	GetController()->ClientSetLocation(location, rot);
	touched = false;
}

void ACubeCharacter::Res()
{
//	this->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
	FRotator rot = FRotator(0, 0, 0);
//	this->TeleportTo(location, rot);
	ChangeColor(rCurCol);

	GetController()->ClientSetLocation(location, rot);
	SetActorLocation(location);
}

void ACubeCharacter::SetPosition(FVector loc){
    location = loc;
}

void ACubeCharacter::ChangeColor(int i){
	UMaterial *materialToSet = materialTwo;
    if(i == even){
        materialToSet = materialTwo;
    }else if(i == evenDying){
        materialToSet = materialTwoDying;
    }else if(i == odd){
        materialToSet = materialOne;
    }else {
        materialToSet = materialOneDying;
    }
    
	curCol = Color(i);
    TArray<UStaticMeshComponent*> Comps;
    
    GetComponents(Comps);
    if(Comps.Num() > 0)
    {
        UStaticMeshComponent* FoundComp = Comps[0];
        FoundComp->SetMaterial(0, materialToSet);
        //do stuff with FoundComp
    }
    
    
}


