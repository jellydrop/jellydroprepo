// Fill out your copyright notice in the Description page of Project Settings.

#include "JellyDrop.h"
#include "PlayerCharacterController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "PlayerCharacter.h"

void APlayerCharacterController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

}

void APlayerCharacterController::CheckRest()
{
	if (!mMoving && (FMath::Abs(mLastFwd) < mDeadsone  && FMath::Abs(mLastRight) < mDeadsone))
		mRest = true;
}

void APlayerCharacterController::Forward()
{
	auto Pawn = GetPawn();
	if (Pawn && !mMoving)
	{
		auto cube = Cast<APlayerCharacter>(Pawn);

		if (!cube->CheckFloor() || CheckForward())
			return;

		auto pos = Pawn->GetActorLocation();

		if (mFacing == Dir::Up)
			Pawn->SetActorLocation(FVector(pos.X + 100, pos.Y, pos.Z));
		else if (mFacing == Dir::Down)
			Pawn->SetActorLocation(FVector(pos.X - 100, pos.Y, pos.Z));
		else if (mFacing == Dir::Right)
			Pawn->SetActorLocation(FVector(pos.X, pos.Y + 100, pos.Z));
		else
			Pawn->SetActorLocation(FVector(pos.X, pos.Y - 100, pos.Z));
	}
}

bool APlayerCharacterController::CheckForward()
{
	FVector Forwad = GetPawn()->GetActorForwardVector();
	FVector StartPos = GetPawn()->GetActorLocation() + Forwad *  75;
	FVector EndPos = StartPos + Forwad * 25;

	return DoTrace(StartPos, EndPos).bBlockingHit; 
}

void APlayerCharacterController::Dash()
{
	if (!mMoving)
	{
		mMoving = true;
		DashForward();
	}
}

void APlayerCharacterController::DashForward()
{
	mDash--;

	if (mDash == 0)
	{
		mMoving = false;
		mDash = 8;
		return;
	}

	FVector Forwad = GetPawn()->GetActorForwardVector();
	FVector StartPos = GetPawn()->GetActorLocation() + Forwad * 75;
	FVector EndPos = StartPos + Forwad * 25;

	auto Hit = DoTrace(StartPos, EndPos);

	auto Pawn = GetPawn();
	Pawn->SetActorLocation(Pawn->GetActorLocation() + Forwad * 100);
	auto cube = Cast<APlayerCharacter>(Pawn);

	if (!cube->CheckFloor())
	{
		Pawn->SetActorLocation(Pawn->GetActorLocation() - Forwad * 100);
		mMoving = false;
		mDash = 8;
	}
	else
	{
		Pawn->SetActorLocation(Pawn->GetActorLocation() - Forwad * 100);
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &APlayerCharacterController::DashForward, 0.07f);

		if (Hit.bBlockingHit)
		{
			int numToPush = 1;
			for (int i = 1; i < 4; i++)
			{
				FVector StartPosl = StartPos + Forwad * (100 * i);
				FVector EndPosl = StartPosl + Forwad * 25;
				auto Hitl = DoTrace(StartPosl, EndPosl);

				if (Hitl.bBlockingHit)
					numToPush++;
				else
					break;
			}

			for (int i = numToPush - 1; i >= 0; i--)
			{
				FVector StartPosl = StartPos + Forwad * (100 * i);
				FVector EndPosl = StartPosl + Forwad * 25;
				auto Hitl = DoTrace(StartPosl, EndPosl);

				auto hitPlayerl = Hitl.GetActor();
				hitPlayerl->SetActorLocation(hitPlayerl->GetActorLocation() + Forwad * 100);
			}
		}
		Pawn->SetActorLocation(Pawn->GetActorLocation() + Forwad * 100);
	}

}

void APlayerCharacterController::ResetGame()
{
	GetGameInstance()->GetFirstLocalPlayerController()->ConsoleCommand(TEXT("RestartLevel"));
}

void APlayerCharacterController::QuitGame(){
	GetGameInstance()->GetFirstLocalPlayerController()->ConsoleCommand(TEXT("quit"));

}

FHitResult APlayerCharacterController::DoTrace(FVector start, FVector end)
{

	FCollisionQueryParams TraceParams;
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(Hit, start, end,
		FCollisionObjectQueryParams::AllObjects, TraceParams);
	
	return Hit;
}

void APlayerCharacterController::Face(Dir::Type dir)
{
	APawn* Pawn = GetPawn();
	if (Pawn && !mMoving)
	{
		auto rot = FRotator(0.0f, 90.0f * dir - 90.0f, 0.0f);
		if (dir == Dir::Right || dir == Dir::Left)
			rot = FRotator(0.0f, 90.0f * dir + 90.0f, 0.0f);
		
		auto cube = Cast<APlayerCharacter>(Pawn);
		cube->Rotate(rot);
		//Pawn->SetActorRelativeScale3D(FVector(1.1f,1.1f,1.1f));
		mFacing = dir;
	}

}

void APlayerCharacterController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &APlayerCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &APlayerCharacterController::MoveRight);

	InputComponent->BindAction("Attack", IE_Pressed, this, &APlayerCharacterController::Attack);
	InputComponent->BindAction("Reset", IE_Pressed, this, &APlayerCharacterController::ResetGame);
	InputComponent->BindAction("Dash", IE_Pressed, this, &APlayerCharacterController::Dash);
	InputComponent->BindAction("QuitGame", IE_Pressed, this, &APlayerCharacterController::QuitGame);
}

void APlayerCharacterController::MoveForward(float Value)
{
	if (Value != 0.0f && mRest && !mMoving)
	{
		APawn* Pawn = GetPawn();
		Dir::Type dr = Dir::None;
		if (Pawn)
		{
			if(Value >= mDeadsone)
				dr = Dir::Up;
			if(Value <= -mDeadsone)
				dr = Dir::Down;
			
			if(dr != Dir::None)
			{
				mRest = false;
				if(dr == mFacing)
					Forward();
				else
					Face(dr);
			}
		}
	}

	mLastFwd = Value;
	CheckRest();
}

void APlayerCharacterController::MoveRight(float Value)
{
	if (Value != 0.0f && mRest && !mMoving)
	{
		APawn* Pawn = GetPawn();
		Dir::Type dr = Dir::None;
		if (Pawn)
		{
			if(Value >= mDeadsone)
				dr = Dir::Right;
			if(Value <= -mDeadsone)
				dr = Dir::Left;
			
			if(dr != Dir::None)
			{
				mRest = false;
				if(dr == mFacing)
					Forward();
				else
					Face(dr);
			}
		}
	}

	mLastRight = Value;
	CheckRest();
}

void APlayerCharacterController::Attack(){
	auto Pawn = GetPawn();

	auto cube = Cast<APlayerCharacter>(Pawn);
	cube->Fire(mFacing);
}
