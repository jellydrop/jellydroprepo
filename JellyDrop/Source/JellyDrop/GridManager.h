// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

#include "GridManager.generated.h"

UCLASS()
class JELLYDROP_API AGridManager : public AActor
{
	GENERATED_BODY()
    enum Color {even, evenDying, odd, oddDying};

    
public:	
	// Sets default values for this actor's properties
	AGridManager();

	void PlayerDied();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    void DropFirstLevel();
    void DropSecondLevel();

    void DropCube(int x, int y);

	class ACubeCharacter* GetCube(int x, int y);
    
protected:
    
    UPROPERTY(EditAnywhere)
    TSubclassOf<ACharacter> CharacterClass;
    
    void DropLevel(int start, int end);
    void DarkenLevel(int start, int end);
	
    class ACubeCharacter*** mGrid;
    
    float posX = -110.0f;
    float posY = -400.0f;
    float posZ = 450.0f;

	int mPlayers = 4;
};
