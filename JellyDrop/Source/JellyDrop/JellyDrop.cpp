// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "JellyDrop.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JellyDrop, "JellyDrop" );

DEFINE_LOG_CATEGORY(LogJellyDrop)
 