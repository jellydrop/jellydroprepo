// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "CubeCharacter.generated.h"

UCLASS()
class JELLYDROP_API ACubeCharacter : public ACharacter
{
	GENERATED_BODY()
    
    enum Color {even, evenDying, odd, oddDying};

public:
	// Sets default values for this character's properties
	ACubeCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void SquareHit();
    void DropThisSquare();
	void DropThisSquarePerm();
	void ResetPosition();
	void Res();
    void ChangeColor(int i);
    void SetPosition(FVector loc);
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialOne;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialTwo;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialTwoDying;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialOneDying;

	bool touched = false;

private:
    FVector location;

	Color curCol;
	Color rCurCol;

	FTimerHandle mResetHandle;
};
