// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"


UCLASS()
class JELLYDROP_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void Rotate(FRotator rot);

	void Fire(int dir);

	bool CheckFloor();

	void SetSpawnCube();

	void SetPlayerNum(int n);

	void CanFall();
    void SetColor(int i);

	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> BallClass;

	int DeathZ = -1000;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lives")
	int32 mLives = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	int32 playerNum;

	bool inv = false;

protected:
	FVector stPos;
	class ACubeCharacter* spawnCube;

	UFUNCTION(BlueprintCallable, Category = "Damage")
		void ResetPlayer();
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialOne;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialTwo;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialThree;
    
    UPROPERTY(EditAnywhere, Category = "Materials")
    UMaterial *materialFour;
};
