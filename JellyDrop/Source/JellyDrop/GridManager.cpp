// Fill out your copyright notice in the Description page of Project Settings.

#include "JellyDrop.h"
#include "Engine/TargetPoint.h"
#include "CubeCharacter.h"
#include "GridManager.h"


// Sets default values
AGridManager::AGridManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AGridManager::PlayerDied()
{
	mPlayers--;
	if (mPlayers == 3)
	{
		DarkenLevel(0, 8);
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &AGridManager::DropFirstLevel, 2.0f);
	}
	else if (mPlayers == 2)
	{
		DarkenLevel(1, 7);
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &AGridManager::DropSecondLevel, 2.0f);
	}
}

void AGridManager::BeginPlay()
{
    Super::BeginPlay();
    mGrid = new ACubeCharacter**[8];
    for(int i =0; i < 8; i++){
        mGrid[i] = new ACubeCharacter*[8];
    }
    
    for(int j = 0; j < 8; j++){
                posY = -400.0f;
        for(int i = 0; i < 8; i++){
            FVector pos = FVector(posX, posY, posZ);
            FRotator rot = FRotator(0, 0, 0);
            ACubeCharacter* Char = GetWorld()->SpawnActor<ACubeCharacter>(CharacterClass, pos, rot);
            
                       mGrid[j][i] = Char;
            mGrid[j][i]->SetPosition(pos);
            mGrid[j][i]->SpawnDefaultController();
            
            if((j+i) % 2 == 1) mGrid[j][i]->ChangeColor(odd);
            else mGrid[j][i]->ChangeColor(even);
            
            //each cube's default movement is set to "NONE" in the blueprint (this is why they do not fall on spawn)
            posY += 100.0f;
        }
        posX -= 100.0f;
    }
    
//    FTimerHandle handle;
//    GetWorldTimerManager().SetTimer(handle, this, &AGridManager::DropFirstLevel, 2.0f);
}

// Called every frame
void AGridManager::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

// Drops outer square of cubes (to be called after first person dies)
void AGridManager::DropFirstLevel(){
    DropLevel(0, 8);
}

// Drops second outer square of cubes (to be called after first person dies);
void AGridManager::DropSecondLevel(){
    DropLevel(1, 7);
}

void AGridManager::DropLevel(int start, int end){
    DarkenLevel(start, end);
    for(int i = start; i < end; i++){
        for(int j = start; j < end; j++){
            if(i == start || i == end-1 || j == start || j == end-1){
				mGrid[i][j]->DropThisSquarePerm();
            }
        }
    }
}

void AGridManager::DarkenLevel(int start, int end){
    for(int i = start; i < end; i++){
        for(int j = start; j < end; j++){
            if(i == start || i == end-1 || j == start || j == end-1){
                if((i+j) % 2 == 1) mGrid[i][j]->ChangeColor(oddDying);
                else mGrid[i][j]->ChangeColor(evenDying);
            }
        }
    }
}

void AGridManager::DropCube(int x, int y){
    mGrid[x][y]->DropThisSquare();
}

ACubeCharacter* AGridManager::GetCube(int x, int y)
{
	return mGrid[x][y];
}

