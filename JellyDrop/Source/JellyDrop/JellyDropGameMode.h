// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "JellyDropGameMode.generated.h"

UCLASS(minimalapi)
class AJellyDropGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AJellyDropGameMode();

	void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<APawn> ActualPawn;

};
