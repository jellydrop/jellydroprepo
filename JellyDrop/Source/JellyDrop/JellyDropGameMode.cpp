// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "JellyDrop.h"
#include "JellyDropGameMode.h"
#include "JellyDropPlayerController.h"
#include "JellyDropCharacter.h"
#include "PlayerCharacterController.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "PlayerCharacter.h"

AJellyDropGameMode::AJellyDropGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AJellyDropPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AJellyDropGameMode::BeginPlay()
{
	Super::BeginPlay();
	// use our custom PlayerController class
	/*PlayerControllerClass = APlayerCharacterController::StaticClass();

	DefaultPawnClass = ActualPawn;
	APlayerController* controller = UGameplayStatics::CreatePlayer(GetWorld(), -1, true);
	controller->GetPawn();*/

	auto game = GetWorld()->GetGameInstance();
		
	for (int i = 1; i < 4; i++)
	{
		FString e;
		auto player = game->CreateLocalPlayer(i, e, true);
		//game->AddLocalPlayer(player, i);
	}

	int i = 0;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		auto pawn = Iterator->Get()->GetPawn();

		if (!pawn) continue;

		auto cube = Cast<APlayerCharacter>(pawn);

		cube->SetPlayerNum(i);
		if (i == 0) pawn->SetActorLocation(FVector(-600, 100, 800));
		if (i == 1) pawn->SetActorLocation(FVector(-300, 100, 800));
		if (i == 2) pawn->SetActorLocation(FVector(-600, -200, 800));
		if (i == 3) pawn->SetActorLocation(FVector(-300, -200, 800));
		i++;
	}
}
