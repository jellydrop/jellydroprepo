// Fill out your copyright notice in the Description page of Project Settings.

#include "JellyDrop.h"
#include "CubeCharacter.h"
#include "AttackBall.h"


// Sets default values
AAttackBall::AAttackBall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAttackBall::BeginPlay()
{
	Super::BeginPlay();/*
	auto XXXComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("XXX"));
	XXXComponent->SetSphereRadius(2000.0f);
	XXXComponent->AttachParent = RootComponent;
	XXXComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	XXXComponent->SetCollisionResponseToChannel(ECC_XXX, ECollisionResponse::ECR_Overlap);

	XXXComponent->OnComponentBeginOverlap.AddDynamic(this, &AXXXCharacter::OnXXXOverlapBegin);
	XXXComponent->OnComponentEndOverlap.AddDynamic(this, &AXXXCharacter::OnXXXOverlapEnd);*/



}

// Called every frame
void AAttackBall::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

UFUNCTION(BlueprintCallable, Category = "SunShine")
void AAttackBall::OnBallOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){
	if (OtherActor->IsA(ACubeCharacter::StaticClass()))
	{
		auto cube = Cast<ACubeCharacter>(OtherActor);

		cube->SquareHit();
		//UE_LOG(LogTemp, Log, TEXT("Poop!"));
	}
}