// Fill out your copyright notice in the Description page of Project Settings.

#include "JellyDrop.h"
#include "PlayerCharacter.h"
#include "AttackBall.h"
#include "GridManager.h"
#include "CubeCharacter.h"
#include <Runtime/Engine/Public/EngineUtils.h>

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	stPos = FVector(GetActorLocation());

	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &APlayerCharacter::SetSpawnCube, 1.0f);
}

// Called every frame
void APlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	auto loc = GetActorLocation();
	if (loc.Z < DeathZ)
	{
		ResetPlayer();
		loc = GetActorLocation();
	}

	if (mLives == 0)
		SetActorLocation(FVector(0, 0, 5000));

	if (inv || CheckFloor())
		SetActorLocation(FVector(loc.X, loc.Y, 530));
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void APlayerCharacter::Rotate(FRotator rot)
{
	SetActorRotation(rot);
	GetMesh()->SetWorldRotation(rot);
	GetController()->SetControlRotation(rot);
}

void APlayerCharacter::Fire(int dir)
{
	FVector pos = GetActorLocation() + GetActorForwardVector() * 100;
	FRotator rot = GetActorRotation();

	auto Char = GetWorld()->SpawnActor<AAttackBall>(BallClass, FVector(pos.X, pos.Y, pos.Z - 100), rot);
}

bool APlayerCharacter::CheckFloor()
{
	if (inv)
		return true;

	FVector StartPos = GetActorLocation() + FVector(0, 0, -100);

	// Calculate end position
	FVector EndPos = GetActorLocation() + FVector(0, 0, -200);/*TODO: Figure out vector math */;
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams;
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	// This fires the ray and checks against all objects w/ collision
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos,
		FCollisionObjectQueryParams::AllObjects, TraceParams);
	// Did this hit anything?

	return Hit.bBlockingHit && Hit.GetActor()->IsA(ACubeCharacter::StaticClass());
}

void APlayerCharacter::SetPlayerNum(int n)
{
	playerNum = n;
    SetColor(n);
}

void APlayerCharacter::CanFall()
{
	inv = false;
}


void APlayerCharacter::SetSpawnCube()
{
	AGridManager* man = nullptr;
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->IsA(AGridManager::StaticClass()))
		{
			man = Cast<AGridManager>(*ActorItr);
		}
	}

	if (playerNum == 0) spawnCube = man->GetCube(5, 5);
	if (playerNum == 1) spawnCube = man->GetCube(2, 5);
	if (playerNum == 2) spawnCube = man->GetCube(5, 2);
	if (playerNum == 3) spawnCube = man->GetCube(5, 5);
}

void APlayerCharacter::ResetPlayer()
{
	AGridManager* man = nullptr;
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->IsA(AGridManager::StaticClass()))
		{
			man = Cast<AGridManager>(*ActorItr);
		}
	}

	mLives--;

	if (mLives > 0)
	{
		inv = true;
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &APlayerCharacter::CanFall, 2.5f);
		SetActorLocation(stPos);
		//spawnCube->ResetPosition();
	}
	else
	{
		SetActorLocation(FVector(0, 0, 5000));
		man->PlayerDied();
	}


}


void APlayerCharacter::SetColor(int i){
    UMaterial *materialToSet = materialTwo;
    if(i == 0){
        materialToSet = materialTwo;
    }else if(i == 1){
        materialToSet = materialThree;
    }else if(i == 2){
        materialToSet = materialOne;
    }else {
        materialToSet = materialFour;
    }
    

    TArray<USkeletalMeshComponent*> Comps;
    
    GetComponents(Comps);
    if(Comps.Num() > 0)
    {
        USkeletalMeshComponent* FoundComp = Comps[0];
        FoundComp->SetMaterial(0, materialToSet);
        //do stuff with FoundComp
    }
    
    
}

